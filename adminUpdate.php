<?php
// This Page is used to update books in the database

// Start the session
session_start();
require 'config.php';
include 'php/bookSelects.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
  <head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    
	<title>In-di-eBooks</title>
	<!-- <link rel="stylesheet" type="text/css" href="css/main.css" /> -->
    <link href="css/jumbotron-narrow.css" rel="stylesheet">
	
   <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet"/>
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
	
</head>

<body>

<?php	
	// If the person is not logged in and on the admin page send them to the log in page
	if(!$_SESSION['loggedIn'] == true)
	{
		header("Location: index.php");
		$err="Please Log in";
		$_SESSION['error'] = $err;		
	}
	
	//If the person is not an admin send them to the login page 
	if(!$_SESSION['admin'] == 1)
	{
		 header("Location:index.php");	
		 $err="Please Log in as an Administrator";
		 $_SESSION['error'] = $err;	
	}	
?>

<body>
	<div class="container">
      <div class="header clearfix">
        <nav>
          <ul class="nav nav-pills pull-right">
			<li role="presentation" ><a href="index.php">Home</a></li>
			<?php	// If the User is logged in as an admin and they have a session for admin value then show them the admin link 
				if(isset($_SESSION['admin']) && $_SESSION['admin'] == 1) { ?>
				<li role="presentation"><a href="admin.php"> Admin </a></li>
				<li role="presentation"><a href= "auditLog.php"> Audit Log </a></li>
			<?php } ?>
				<li role="presentation"><a href="users.php"> My Profile </a></li>
				<li role="presentation"><a href="logout.php"> Logout </a></li>
          </ul>
        </nav>
        <h3 class="text-muted">In-di-eBooks</h3>
      </div>

<?php
// check if there is an message stored that needs to be output
	if(isset($_SESSION['error']))
	{
		echo '<div class="err">'.$_SESSION['error'].'</div>';
		unset($_SESSION['error']);
	}
	$err = "";

	$result = selectCurrentBook($_GET['id']); // Get the current book details
	while($row = mysqli_fetch_array($result))
	{
		$bookId = $row['BOOK_ID'];
		$title = $row['TITLE'];
		$author =$row['AUTHOR'];
		$year = $row['YEAR'];
		$description = $row['DESCRIPTION'];
		$price = $row['PRICE'];
	}  

?>
	<form action="php/admin.php?Action=update" enctype="multipart/form-data" method="POST">
		<input type="hidden" name="bookid" id="bookid" value= "<?php echo $bookId ?>"  />
		<label>Title</label>
		<input type="text" name="title" id="title" value = "<?php echo $title ?>"  />
		<label>Author</label>
		<input type="text" name="author" id="author" value = "<?php echo $author ?>" />
		<label>Year:</label>
		<input type="text" name="year" id="year"value = "<?php echo $year ?>" />
		<label>Description:</label>
		<input type="text" name="description" id="description" value = "<?php echo $description ?>" />
		<label>Price</label>
		<input type="text" name="price" id="price" value =  "<?php echo $price ?>" />
		<label for="file">Upload New Version of Book(Optional):</label>
		<input type="file" name="file" id="file"><br>
		<input type="submit" name="UpdateBook" value="Update Book" />
	</form>

</body>       
</html>