-- phpMyAdmin SQL Dump
-- version 2.8.0.1
-- http://www.phpmyadmin.net
-- 
-- Host: custsql-ipg45.eigbox.net
-- Generation Time: Apr 30, 2015 at 01:12 PM
-- Server version: 5.5.32
-- PHP Version: 4.4.9
-- 
-- Database: `indiebooks_1`
-- 
CREATE DATABASE `indiebooks_1` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `indiebooks_1`;

-- --------------------------------------------------------

-- 
-- Table structure for table `AUDIT`
-- 

CREATE TABLE `AUDIT` (
  `AUDITKEY` varchar(255) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- 
-- Dumping data for table `AUDIT`
-- 

INSERT INTO `AUDIT` VALUES ('cc3fdaea63d906ec71aec9c296dc957b12597e06');

-- --------------------------------------------------------

-- 
-- Table structure for table `AUDITLOG`
-- 

CREATE TABLE `AUDITLOG` (
  `AUDIT_ID` int(11) NOT NULL AUTO_INCREMENT,
  `DOWNLOADS` int(11) NOT NULL,
  `USER_ID` int(11) NOT NULL,
  `BOOK_ID` int(11) NOT NULL,
  `DATETIME` datetime NOT NULL,
  `HASH` varchar(255) NOT NULL,
  `SIGNATURE` varchar(255) NOT NULL,
  PRIMARY KEY (`AUDIT_ID`),
  KEY `USER_ID` (`USER_ID`),
  KEY `BOOK_ID` (`BOOK_ID`)
) ENGINE=MyISAM AUTO_INCREMENT=43 DEFAULT CHARSET=latin1 AUTO_INCREMENT=43 ;

-- 
-- Dumping data for table `AUDITLOG`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `BOOKS`
-- 

CREATE TABLE `BOOKS` (
  `BOOK_ID` int(11) NOT NULL AUTO_INCREMENT,
  `TITLE` varchar(100) NOT NULL,
  `AUTHOR` varchar(50) NOT NULL,
  `YEAR` int(4) DEFAULT NULL,
  `DESCRIPTION` varchar(1000) DEFAULT NULL,
  `PRICE` decimal(10,2) NOT NULL DEFAULT '0.00',
  `FILENAME` varchar(500) NOT NULL DEFAULT '',
  PRIMARY KEY (`BOOK_ID`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

-- 
-- Dumping data for table `BOOKS`
-- 

INSERT INTO `BOOKS` VALUES (1, 'HeadHunters', 'Jo Nesbo', 2012, 'Roger Brown has it all: clever and wealthy, he''s at the very top of his game. And if his job as a headhunter ever gets dull, he has his sideline as an art thief to keep him busy.  At a gallery opening, his wife introduces him to Clas Greve. Not only is Greve the perfect candidate for a position that Brown is recruiting for; he is also in possession of one of the most sought-after paintings in mode', 3.86, 'HeadHunters.2012');
INSERT INTO `BOOKS` VALUES (2, 'The Hobbit', 'J.R.R Tolkien', 1937, 'Bilbo Baggins is a hobbit who enjoys a comfortable, unambitious life, rarely travelling further than the pantry of his hobbit-hole in Bag End.  But his contentment is disturbed when the wizard, Gandalf, and a company of thirteen dwarves arrive on his doorstep one day to whisk him away on an unexpected journey ‘there and back again’. They have a plot to raid the treasure hoard of Smaug the Magnific', 3.84, 'The Hobbit.1937');
INSERT INTO `BOOKS` VALUES (6, 'Game of Thrones', 'George R.R Martin', 2013, 'A STORM OF SWORDS: STEEL AND SNOW is the FIRST part of the third volume in the series.  Winter approaches Westeros like an angry beast.  The Seven Kingdoms are divided by revolt and blood feud. In the northern wastes, a horde of hungry, savage people steeped in the dark magic of the wilderness is poised to invade the Kingdom of the North where Robb Stark wears his new-forged crown. And Robb’s defences are ranged against the South, the land of the cunning and cruel Lannisters, who have his younger sisters in their power.  Throughout Westeros, the war for the Iron Throne rages more fiercely than ever, but if the Wall is breached, no king will live to claim it.', 3.85, 'Game of Thrones.2013');
INSERT INTO `BOOKS` VALUES (7, 'Phantom: A Harry Hole thriller', 'Nesbo Jo', 2013, 'After the horrors of a case that nearly cost him his life, Harry Hole left Oslo and the police force far behind him. Now he''s back, but the case he''s come to investigate is already closed, and the suspect already behind bars.  THE POLICE DON''T WANT HIM BACK...  Denied permission to reopen the investigation, Harry strikes out on his own, quickly discovering a trail of violence and mysterious disappearances apparently unnoticed by the police. At every turn, Harry is faced with a wall of silence. ', 3.84, 'Phantom: A Harry Hole thriller.2013');

-- --------------------------------------------------------

-- 
-- Table structure for table `BOOKSOWNED`
-- 

CREATE TABLE `BOOKSOWNED` (
  `BOOKOWNED_ID` int(11) NOT NULL AUTO_INCREMENT,
  `USER_ID` int(11) DEFAULT NULL,
  `BOOK_ID` int(11) DEFAULT NULL,
  `LINK` varchar(500) NOT NULL,
  `CONFIRMED` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`BOOKOWNED_ID`),
  KEY `USER_ID` (`USER_ID`),
  KEY `BOOK_ID` (`BOOK_ID`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

-- 
-- Dumping data for table `BOOKSOWNED`
-- 

INSERT INTO `BOOKSOWNED` VALUES (1, 4, 1, 'de8af113744ca70c9222c5840de91b', 1);
INSERT INTO `BOOKSOWNED` VALUES (2, 4, 2, '8a0156e4388ca3d53b2945cd1ddaa2', 1);
INSERT INTO `BOOKSOWNED` VALUES (3, 2, 1, 'efa02d010b41945ff39e5e1d9c2c95', 1);
INSERT INTO `BOOKSOWNED` VALUES (4, 1, 1, '248b47b70eaba48769846125257b3b', 1);
INSERT INTO `BOOKSOWNED` VALUES (5, 1, 2, '1de681ba1c4e99f0faa889265150fc', 1);
INSERT INTO `BOOKSOWNED` VALUES (6, 1, 6, '5382bbb3f053af2dbe215293d769c7', 1);
INSERT INTO `BOOKSOWNED` VALUES (7, 1, 7, '834313218170e1df96a08b94e96f1b', 1);
INSERT INTO `BOOKSOWNED` VALUES (8, 2, 2, 'c4edd9b1e806a6484f6d7bcbd9048a', 1);

-- --------------------------------------------------------

-- 
-- Table structure for table `REVIEWS`
-- 

CREATE TABLE `REVIEWS` (
  `REVIEW_ID` int(11) NOT NULL AUTO_INCREMENT,
  `CONTENT` varchar(500) NOT NULL,
  `RATING` int(1) DEFAULT NULL,
  `USER_ID` int(11) DEFAULT NULL,
  `BOOK_ID` int(11) DEFAULT NULL,
  PRIMARY KEY (`REVIEW_ID`),
  KEY `USER_ID` (`USER_ID`),
  KEY `BOOK_ID` (`BOOK_ID`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

-- 
-- Dumping data for table `REVIEWS`
-- 

INSERT INTO `REVIEWS` VALUES (1, ' This book is awesome but abit short', 4, 1, 1);
INSERT INTO `REVIEWS` VALUES (2, ' This book is stranggeeeee', 4, 2, 1);
INSERT INTO `REVIEWS` VALUES (3, ' I was not a Fan of this book ', 1, 1, 1);
INSERT INTO `REVIEWS` VALUES (4, ' index.php?name=guest<script>alert(''attacked'')</script>', 0, 1, 1);
INSERT INTO `REVIEWS` VALUES (5, 'I need to read this book!!', 5, 1, 7);
INSERT INTO `REVIEWS` VALUES (6, ' Mannn I haven''t read this since I was like 5', 5, 1, 2);

-- --------------------------------------------------------

-- 
-- Table structure for table `USERS`
-- 

CREATE TABLE `USERS` (
  `USER_ID` int(11) NOT NULL AUTO_INCREMENT,
  `USERNAME` varchar(40) NOT NULL DEFAULT '',
  `FIRSTNAME` varchar(40) NOT NULL DEFAULT '',
  `LASTNAME` varchar(40) NOT NULL DEFAULT '',
  `PASSWORD` char(60) NOT NULL DEFAULT '',
  `EMAIL` varchar(80) NOT NULL DEFAULT '',
  `ADMIN` tinyint(1) NOT NULL DEFAULT '0',
  `SALT` char(21) NOT NULL,
  PRIMARY KEY (`USER_ID`)
) ENGINE=MyISAM AUTO_INCREMENT=17 DEFAULT CHARSET=latin1 AUTO_INCREMENT=17 ;

-- 
-- Dumping data for table `USERS`
-- 

INSERT INTO `USERS` VALUES (1, 'Admin', 'Admin', 'strator', '$2a$05$rvLzj5vwoXiCuQvll6MjE.tvsGE3vUi84ciYEVzphT1ca2xyfDtCK', 'asdasd', 1, 'rvLzj5vwoXiCuQvll6MjE');
INSERT INTO `USERS` VALUES (2, 'Test', 'Test', 'ing', '$2a$05$7N.PSdpglkIvVsvMc/2Af.IivxPrq07mG1VSgGLgzZ8wTTrYGJNwm', 'test', 0, '7N.PSdpglkIvVsvMc/2Af');
INSERT INTO `USERS` VALUES (4, 'user ', 'us', 'er', '$2a$05$3cy.e8gR9nYJwXy7kG0Kb.PVilc/4ID/zo9DK3p8rSBwg0bmlndcu', 'asdasd', 0, '3cy.e8gR9nYJwXy7kG0Kb');
INSERT INTO `USERS` VALUES (10, 'FinalTest ', 'Final', 'Test', '$2a$05$75ti2L29l1B2Izv03W32u.cR2cP4iFJ6nz7a5CRwUYAoih2g1oMU6', 'test', 0, '75ti2L29l1B2Izv03W32u');
INSERT INTO `USERS` VALUES (11, 'hello', 'test', 'ing', '$2a$05$XSf4OlZEFwnDYZ.x6LuwX.qaBgc1PbNGYftx7Sa0crp3qcmd.QvlW', 'hello', 0, 'XSf4OlZEFwnDYZ.x6LuwX');
INSERT INTO `USERS` VALUES (12, 'sh', 'Steven', 'Hanson', '$2a$05$2m2rE7pDd4A4m6ArPEfGa.mWhh7HWxbWf1f0LLrksYf2NR3eFaoue', 'stevenahanson@live.com', 0, '2m2rE7pDd4A4m6ArPEfGa');
INSERT INTO `USERS` VALUES (13, 'Hello', 'Test', 'Hello', '$2a$05$s4B7zVgSgNfSbyeglt9dZ.v4pHt1/ogjJntNLUNoVU2pjrIx2ddOu', 'Testmail', 0, 's4B7zVgSgNfSbyeglt9dZ');
INSERT INTO `USERS` VALUES (14, 'sk35', 'Steven', 'Long', '$2a$05$5zgRf2SCKInRwLp/rCD8R.FBoA/gwq.hRcMTRKJCD1JZcXyWZQLMG', 'steven91@gmail.com', 0, '5zgRf2SCKInRwLp/rCD8R');
