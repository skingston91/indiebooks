<?php
// Code for the google checkout 

// Starting the session
session_start();
require 'config.php';

// Include all the required files
require_once('library/googlecart.php');
require_once('library/googleitem.php');
require_once('library/googleshipping.php');
require_once('library/googletax.php');

Usecase();
function Usecase() {

  $title = $_POST['title'];
  $author = $_POST['author']; // Issue if they contain spaces 
  $link = substr(md5(rand()), 0, 30); // Create a random link for the user
  $_SESSION['link'] = $link;
  $price = $_POST['price'];
  $bookid = $_POST['bookid'];

  $merchant_id = constant("MERCHANT_ID");  // Your Merchant ID
  $merchant_key = constant("MERCHANT_KEY");  // Your Merchant Key
  $server_type = "sandbox";
  $currency = $_POST['currency'];
  $cart = new GoogleCart($merchant_id, $merchant_key, $server_type,
  $currency);
  $total_count = $_POST['quantity'];
  $certificate_path = __DIR__ . "/ssl/mozilla.pem";  // set your SSL CA cert path

//  Key/URL delivery
// it is title, description, total bought
  $item_1 = new GoogleItem($title,  
                           $author, 
                           $total_count, 
                           $price); // Unit price

// Sets a Relative Location for the purchase page
  $item_1->SetURLDigitalContent('http://'.$_SERVER['HTTP_HOST'].dirname($_SERVER['PHP_SELF']).'/confirmPurchase.php?id=' . $bookid . '',
                                $link,
                                $title);
	
  $cart->AddItem($item_1); 
  
// Specify "Return to xyz" link
  $cart->SetContinueShoppingUrl('http://'.$_SERVER['HTTP_HOST'].dirname($_SERVER['PHP_SELF']).'/confirmPurchase.php?id=' . $bookid .  '');
  
   
// This will do a server-2-server cart post and send an HTTP 302 redirect status
// This is the best way to do it if implementing digital delivery
// More info http://code.google.com/apis/checkout/developer/index.html#alternate_technique
  list($status, $error) = $cart->CheckoutServer2Server('', $certificate_path);
  // if i reach this point, something was wrong
  echo "An error had ocurred: <br />HTTP Status: " . $status. ":";
  echo "<br />Error message:<br />";
  echo $error;
//

}
?>