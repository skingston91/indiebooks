<?php
// This is the page which outputs the users books
// Starting the session
session_start();
require 'config.php';
include 'php/bookSelects.php';

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
  <head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    
	<title>In-di-eBooks</title>
	<!-- <link rel="stylesheet" type="text/css" href="css/main.css" /> -->
    <link href="css/jumbotron-narrow.css" rel="stylesheet">
	
   <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet"/>
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
	
</head>

<body>
	<div class="container">
      <div class="header clearfix">
        <nav>
          <ul class="nav nav-pills pull-right">
			<li role="presentation" ><a href="index.php">Home</a></li>
			<?php	// If the User is logged in as an admin and they have a session for admin value then show them the admin link 
				if(isset($_SESSION['admin']) && $_SESSION['admin'] == 1) { ?>
				<li role="presentation"><a href="admin.php"> Admin </a></li>
				<li role="presentation"><a href= "auditLog.php"> Audit Log </a></li>
			<?php } ?>
			<?php	// If the user is logged in then don't output the login and sign in links	
				if(!isset($_SESSION['loggedIn'])) { 
				$err = 'Please Login to see your books!'; // add the error
				header("Location: index.php"); // send them back to the log in page
				$_SESSION['error'] = $err; // set the error as the session
				}
			else //if they logged in show them the logOut link
			{	?>
				<li role="presentation" class ="active"><a href="users.php"> My Profile </a></li>
				<li role="presentation"><a href="logout.php"> Logout </a></li>
			<?php } ?>
        </nav>
        <h3 class="text-muted">In-di-eBooks</h3>
	</div>
	  
<?php
	// Output the Error if there is one
	if(isset($_SESSION['error']))
	{
		echo '<div class="err">'.$_SESSION['error'].'</div>';
		unset($_SESSION['error']);
	}

		$session = $_SESSION['userid'];
		
		$result = getUserBooks($session);
		$row_cnt = mysqli_num_rows($result); // count the amount of rows
		if($row_cnt == 0)
		{
			echo"Sorry you don't own any books please buy some!!";
		}

		// Output all the users owned books in the database 
		$result = getAllBoughtBooks($session);
		while($row = mysqli_fetch_array($result))
		{	
			echo  htmlspecialchars($row['TITLE']) . " " .  htmlspecialchars($row['AUTHOR']). " " .  htmlspecialchars($row['YEAR']). " ";
			echo ' <a href=books/download.php?id='. htmlspecialchars($row['BOOK_ID']).'&Link='. $row['LINK'].'> Download Link </a>';
			echo "<br />";
		}

		// Output all the users owned books in the database 
		$result = getAllUnverfiedBooks($session);
		$row_cnt = mysqli_num_rows($result); // count the amount of rows
		
		while($row = mysqli_fetch_array($result))
		{	
			echo  htmlspecialchars($row['TITLE']) . " " .  htmlspecialchars($row['AUTHOR']). " " .  htmlspecialchars($row['YEAR']). " ";
			echo ' <a href=verficiation.php?id='. htmlspecialchars($row['BOOK_ID']).'> Verify</a>';
			echo "<br />";
		}
	
?>	