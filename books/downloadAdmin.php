<?php
// The download file is located in the books area because security reasons,
// as didn't want to use relative file path redirects because of attempts to download
// different files on the system

// Start the session
session_start();
require '../config.php';
$err = "";
	if(isset($_SESSION['admin']) && $_SESSION['admin'] == 1)
	{
		$dbh = connectToDatabase();
		$bookid= $dbh ->real_escape_string($_GET['id']);
		$admin = $_SESSION['admin'];
		
		$result = $dbh->query("SELECT FILENAME,BOOK_ID FROM BOOKS 
			WHERE BOOK_ID = '$bookid'");
			
		$row_cnt = mysqli_num_rows($result); // count the amount of rows	
			$row = $result->fetch_assoc();	
			$filename = $row['FILENAME'];
			$bookId = $row['BOOK_ID'];
			//content type
			header('Content-type: text/plain');
			//open/save dialog box
			header("Content-Disposition: attachment; filename='$filename.txt'");
			//read from server and write to buffer
			readfile("$bookId.txt");
			disconnectFromDatabase($dbh);		
	}
	else 
	{
		$err = 'You are not an admin'; // add the error
		header("Location: ../index.php"); // send them back to the log in page
		$_SESSION['error'] = $err; // set the error as the session
		disconnectFromDatabase($dbh);	
	}	
?>	