<?php
include '../php/audit.php';

// The download file is located in the books area because security reasons,
// as didn't want to use relative file path redirects because of attempts to download
// different files on the system

// Start the session
session_start();
require '../config.php';
$err = "";
	$dbh = connectToDatabase();
	$bookid= $dbh ->real_escape_string($_GET['id']);
	$downloadLink= $dbh ->real_escape_string($_GET['Link']);
	$session = $dbh ->real_escape_string($_SESSION['userid']);

	// Make sure that the download link matchs the users session, the books id matches and that the linkid matches the book owned link stored 
	$result = $dbh->query("SELECT b.FILENAME,b.BOOK_ID FROM BOOKS b
		INNER JOIN BOOKSOWNED bo
		ON b.BOOK_ID = bo.BOOK_ID
		INNER JOIN USERS u
		ON bo.USER_ID = u.USER_ID
		WHERE u.USER_ID = '$session'
		AND bo.BOOK_ID = '$bookid'
		AND LINK = '$downloadLink'
		AND CONFIRMED ='1'");
		
	$row_cnt = mysqli_num_rows($result); // count the amount of rows	
	if($row_cnt == 1) // if its one then set the variables as matching pass and user
	{
		$row = $result->fetch_assoc();	
		$filename = $row['FILENAME'];
		$bookId = $row['BOOK_ID'];
		
		//content type
		header('Content-type: text/plain');
		//open/save dialog box
		header("Content-Disposition: attachment; filename=$filename.txt");// Change the name of the file to the filename
		//read from server and write to buffer
		readfile("$bookId.txt"); // get the file using its ID

		addDownloadAudit($session,$bookId);
		changeAuditKey();
		disconnectFromDatabase($dbh);	

	}
	else 
	{
		$err = 'You need to log in as the owner of the book!'; // add the error
		header("Location: ../index.php"); // send them back to the log in page
		$_SESSION['error'] = $err; // set the error as the session
		disconnectFromDatabase($dbh);	
	}
	
?>	