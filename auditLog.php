<?php
// Starting the session
session_start();
require 'config.php';
include 'php/audit.php';
/*
	auditLog.php - This page deals with outputing any errant logs
*/
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
  <head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    
	<title>In-di-eBooks</title>
	<!-- <link rel="stylesheet" type="text/css" href="css/main.css" /> -->
    <link href="css/jumbotron-narrow.css" rel="stylesheet">
	
   <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet"/>
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
	
</head>

<body>
<?php	
	// If the person is not logged in and on the admin page send them to the log in page
	if(!$_SESSION['loggedIn'] == true)
	{
		header("Location: index.php");
		$err="Please Log in";
		$_SESSION['error'] = $err;		
	}
	
	//If the person is not an admin send them to the login page 
	if(!$_SESSION['admin'] == 1)
	{
		 header("Location:index.php");	
		 $err="Please Log in as an Administrator";
		 $_SESSION['error'] = $err;	
	}	
?>

<body>
	<div class="container">
      <div class="header clearfix">
        <nav>
          <ul class="nav nav-pills pull-right">
			<li role="presentation" ><a href="index.php">Home</a></li>
			<?php	// If the User is logged in as an admin and they have a session for admin value then show them the admin link 
				if(isset($_SESSION['admin']) && $_SESSION['admin'] == 1) { ?>
				<li role="presentation"><a href="admin.php"> Admin </a></li>
				<li role="presentation" class ="active"><a href= "auditLog.php"> Audit Log </a></li>
			<?php } ?>
				<li role="presentation"><a href="users.php"> My Profile </a></li>
				<li role="presentation"><a href="logout.php"> Logout </a></li>
          </ul>
        </nav>
        <h3 class="text-muted">In-di-eBooks</h3>
      </div>

<?php
	// check if there is an message stored that needs to be output
	if(isset($_SESSION['error']))
	{
		echo '<div class="err">'.$_SESSION['error'].'</div>';
		unset($_SESSION['error']);
	}
	$err = "";

	echo "<h4>These are all the excessive Downloaders which have a book with Over 20 downloads</h4><br/>";
	$result = getExcessiveDownloaders();

	while($row = mysqli_fetch_array($result))
	{
		echo " USER ID " . $row['USER_ID'] . " Book ID " . $row['BOOK_ID']." Downloads " . $row['DOWNLOADS'];
		echo "<br>";
	}
	echo "<h4>These are all the errored Logs</h4><br/>";
	$result = getAllAuditLogs();
	while($row = mysqli_fetch_array($result))
	{
		$auditId = $row['AUDIT_ID'];
		$userId = $row['USER_ID'];
		$bookId = $row['BOOK_ID'];
		$dateTime = $row['DATETIME'];
		$orginalHash = $row['HASH']; 
		$downloads = $row['DOWNLOADS'];
		if ($auditId == 1)
		{
			$lastHash ="";
		}

		$result = getUserLastDownloadAmount($userId,$bookId,$auditId);
		while($row = mysqli_fetch_array($result))
		{
			$downloads = $row['DOWNLOADS'];
		}

		if(!isset($downloads))
		{
			$downloads = 0;
		}
		$entry = "$dateTime,$downloads"; // Creates an entry specific to this event
		$entryHash = sha1($lastHash.$entry); // creates the hash off the event 

		if($orginalHash == $entryHash)
		{
			
		}
		else
		{
			echo "Did Not Match:";
			echo " Audit_Id: $auditId";
			echo " User_Id: $userId";
			echo " Book_Id: $bookId";
			echo " Date and time: $dateTime";
			echo " Downloads: $downloads";
			echo " Orginal Hash: $orginalHash";
			echo " Entry Hash: $entryHash";
			echo "<br/>";
				
			echo " Last Audit_Id: $lastAuditId";
			echo " Last User_Id: $lastUserId";
			echo " Last Book_Id: $lastBookId ";
			echo " Last Downloads: $downloads";
			echo " Last Hash: $lastHash";
			echo "<br/>";
			echo "<br/>";
		}
		
		$lastAuditId = $auditId;
		$lastUserId = $userId;
		$lastBookId = $bookId;
		$lastDateTime = $dateTime;
		$lastHash = $orginalHash; // Gets the Last hash entered into the database 	
	}
	
	echo "<h4>These are all the audit Logs</h4><br/>";
	$result = getAllAuditLogs();
	
	while($row = mysqli_fetch_array($result))
	{	
		echo " USER ID " . htmlspecialchars($row['USER_ID']) . " Book ID " . htmlspecialchars($row['BOOK_ID']) . " Downloads " . htmlspecialchars($row['DOWNLOADS']);
		echo " Date and Time " . htmlspecialchars($row['DATETIME']) ;
		echo "<br>"; 
	}
	
	
?>