<?php
// Confirms the purchase of the Newly bought book and updates the audit log 
// Starting the session
session_start();
require 'config.php';
include 'php/verfiy.php';
include 'php/audit.php';
include 'php/bookSelects.php';
include 'php/bookInserts.php';

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
  <head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    
	<title>In-di-eBooks</title>
	<!-- <link rel="stylesheet" type="text/css" href="css/main.css" /> -->
    <link href="css/jumbotron-narrow.css" rel="stylesheet">
	
   <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet"/>
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
	
</head>

<body>
	<div class="jumbotron">
        <h1>In-di-eBooks</h1>
        <p class="lead">Welcome to the In-di-eBooks Store</p>
        <p class="lead">This is a place for people to view books and add reviews on them</p>
		
    </div>
	
	<div class="container">
      <div class="header clearfix">
        <nav>
          <ul class="nav nav-pills pull-right">
			<li role="presentation" ><a href="index.php">Home</a></li>
			<?php	// If the User is logged in as an admin and they have a session for admin value then show them the admin link 
				if(isset($_SESSION['admin']) && $_SESSION['admin'] == 1) { ?>
				<li role="presentation"><a href="admin.php"> Admin </a></li>
				<li role="presentation"><a href= "auditLog.php"> Audit Log </a></li>
			<?php } ?>
			<?php	// If the user is logged in then don't output the login and sign in links	
				if(!isset($_SESSION['loggedIn'])) { ?>
				<li role="presentation"><a href="login.php">Login </a></li>
				<li role="presentation"><a href="signUp.php"> Sign Up </a></li>
			<?php }
			else //if they logged in show them the logOut link
			{	?>
				<li role="presentation"><a href="users.php"> My Profile </a></li>
				<li role="presentation"><a href="logout.php"> Logout </a></li>
			<?php } ?>
          </ul>
        </nav>
        <h3 class="text-muted">In-di-eBooks</h3>
      </div>
	  
<?php
// check if there is an message stored that needs to be output
	if(isset($_SESSION['error']))
	{
		echo '<div class="err">'.$_SESSION['error'].'</div>';
		unset($_SESSION['error']);
	}
	$err = "";

	// If not logged in they shouldn't be at the page
	if(!isset($_SESSION['loggedIn'])) 
	{
		$err = 'You can not have bought a book without logging in'; // add the error
		header("Location: login.php"); // send them back to the log in page
		$_SESSION['error'] = $err; // set the error as the session
	}	
	else 
	{

	}
	
	$userId = $_SESSION['userid']; // gets user id
	if(isset($_GET['id'])) 
	{	
		$bookId = $_GET['id']; //  // if the id is set then put it as the varible 
	}
	elseif(!isset($_GET['id'])) // if its not there then take it from the posted version from the form
	{
		$bookId = $_POST['id'];
	}

	$link = $_SESSION['link']; // Gets the link created before the checkout process
	
	$result = selectCheckBookOwned($bookId,$userId);
	$row = $result->fetch_assoc();
	$row_cnt = mysqli_num_rows($result); // count the amount of rows
	if($row_cnt == 0)
	{
		// If they have got to this page then they have bought a book but unconfirmed it anyone who hasn't wont have a link to enter
		insertUnConfirmedBook($userId,$bookId,$link);
	} 	

	// This select is to stop people reverifiying already verified books
	$result = selectCheckBookConfirmed($bookId,$userId);
	$row = $result->fetch_assoc();
	$row_cnt = mysqli_num_rows($result); // count the amount of rows
	if($row_cnt > 0 && $row['CONFIRMED'] == 1 ) // if its more than zero then it already exists so send them to the book owned
	{	
		$err = 'Please Download Bought Books from your Download Page'; // add the error
		header("Location: users.php");
		$_SESSION['error'] = $err; // set the error as the session
		exit;
	}

	
	if(!isset($_POST['serial'])) // show the verfiy form if nothing in the field 
	{
		verfiyForm($bookId,"confirmPurchase.php");
	}
	
	// if the given serial key is set and matches the database link 
	if(isset($_POST['serial']) && $_POST['serial'] == $_SESSION['link'] ) 
	{
		$result = selectCheckKeyMatch($bookId,$userId,$link); // check that it doesn't already exist and re-add
		$row = $result->fetch_assoc();
		$row_cnt = mysqli_num_rows($result); // count the amount of rows	
		if($row_cnt > 0 && $row['CONFIRMED'] == 1 ) // if its more than zero then it already exists so do nothing to the database
		{
			
		}
		else
		{	
			// Add that the book has been bought and confirmed
			
			updateToConfirmedBook($userId,$bookId,$link);
			addNewBookAudit($userId,$bookId,$link); // add that a new books has been bought to the audit
			changeAuditKey();// Changes the audit key 
	
		}
	}

	elseif(!isset($_POST['serial']))
	{
	
	}	
	elseif($_POST['serial'] != $_SESSION['link']) // if it doesn't match then give it an error and reoutput the form
	{
		echo"Wrong Key";
		
		verfiyForm($bookId,"confirmPurchase.php");
	}	
?>

<?php 
	
	// Output the bought book
	$result = selectBoughtBook($userId,$link); // when its been set to be a confirmed then show the book 
	while($row = mysqli_fetch_array($result))
	{				
		echo  htmlspecialchars($row['TITLE']) . " " .  htmlspecialchars($row['AUTHOR']). " " .  htmlspecialchars($row['YEAR']). " ";
		echo ' <a href=books/download.php?id='. htmlspecialchars($row['BOOK_ID']).'&Link='. htmlspecialchars($row['LINK']).'> Download Link </a>';
		
	}

?>