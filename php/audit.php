<?php
$err = "";
//Functions for dealing with the audit logs 

// Get all the Audit Logs 
function getAllAuditLogs()
{
	if(!$_SESSION['admin'] == 1)
	{
     		exit('<h2>You cannot access this function directly!</h2>');
	}
	
	$dbh = connectToDatabase();
	$result = $dbh->query("SELECT * FROM AUDITLOG");
	return $result;
	disconnectFromDatabase($dbh);
}

// Get all the last downloadAmount
function getUserLastDownloadAmount($userId,$bookId,$auditId)
{
	if(!$_SESSION['admin'] == 1)
	{
     		exit('<h2>You cannot access this function directly!</h2>');
	}
	
	$dbh = connectToDatabase();
	$result = $dbh->query("SELECT * FROM AUDITLOG WHERE USER_ID ='$userId' AND BOOK_ID = '$bookId'
	AND AUDIT_ID < '$auditId' order by AUDIT_ID LIMIT 1"); 
	return $result;
	disconnectFromDatabase($dbh);
}

// Get the highest downloaded book by a user which over 20 downloads
function getExcessiveDownloaders()
{
	if(!$_SESSION['admin'] == 1)
	{
     		exit('<h2>You cannot access this function directly!</h2>');
	}
	
	$dbh = connectToDatabase();
	$result = $dbh->query("SELECT al.*
	FROM AUDITLOG al
	INNER JOIN
    	(
    	SELECT USER_ID, MAX(DOWNLOADS) AS MaxDOWNLOAD
    	FROM AUDITLOG
    	GROUP BY USER_ID
    	) user_idal ON al.USER_ID = user_idal.USER_ID AND al.DOWNLOADS = user_idal.MaxDOWNLOAD HAVING DOWNLOADS > 20");
	return $result;
	disconnectFromDatabase($dbh);
}


// Deals with adding a new book into the audit log 
function addNewBookAudit($userId,$bookId,$link)
{	
	if(!$_SESSION['userid'] == $userId)
	{
     		exit('<h2>You cannot access this function directly!</h2>');
	}
	$dbh = connectToDatabase();
	//Code for updating the log 
	$userId = $dbh ->real_escape_string($userId);
	$bookId = $dbh ->real_escape_string($bookId);
	$link = $dbh ->real_escape_string($link);
	$dateTime = date('Y-m-d H:i:s' );	//Gets the date and time of the event
	$downloads = 0;// just been added to the log so only 0 
	// Gets the last hashvalue inserted into the database
	$result = $dbh ->query("SELECT HASH,MAX(AUDIT_ID)FROM AUDITLOG GROUP BY AUDIT_ID ORDER BY DATETIME DESC LIMIT 1"); 
	while($row = mysqli_fetch_array($result))
	{	
		$lastHash = $row['HASH']; // Gets the Last hash entered into the database 
	}
	if (!isset($lasthash))
	{
		$lastHash="";
	}


	$entry = "$dateTime,$downloads"; // Creates an entry specific to this event
	$entryHash = sha1($lastHash.$entry); // creates the hash off the event 
	
	$result = $dbh->query("SELECT * FROM AUDIT"); 
	$row = $result->fetch_assoc();
	$currentKey = $row['AUDITKEY']; // get the current Audit key out of the database 
	
	// Note First key was 5bf1fd927dfb8679496a2e6cf00cbe50c1c87145
	// Creates a hashed log into the log 
	$result = $dbh ->query("INSERT INTO AUDITLOG (DOWNLOADS,USER_ID,BOOK_ID, DATETIME,HASH,
	SIGNATURE) VALUES ('$downloads','$userId','$bookId','$dateTime','$entryHash', AES_ENCRYPT('$entryHash','$currentKey'))");
	disconnectFromDatabase($dbh);
}
// Adds that file has been downloaded into the audit log
function addDownloadAudit($session,$bookid)
{	
	if(!$_SESSION['userid'] == $session)
	{
     		exit('<h2>You cannot access this function directly!</h2>');
	}
	$dbh = connectToDatabase();
	$session = $dbh ->real_escape_string($session);
	$bookid = $dbh ->real_escape_string($bookid);
	//Code for updating the log from this point
	$dateTime = date('Y-m-d H:i:s' );	//Gets the date and time of the event
	
	// Get what the last amount of downloads was for the book being downloaded 
	$result = $dbh->query("SELECT * FROM AUDITLOG WHERE USER_ID ='$session' AND BOOK_ID = '$bookid'
	ORDER BY DATETIME DESC LIMIT 1"); 
	while($row = mysqli_fetch_array($result))
	{	
		$downloads = $row['DOWNLOADS'];// Gets the old amount of downloads the file has had and incrementes it 
	}
	$downloads++;	
	
	// Gets the last hashvalue inserted into the database
	$result = $dbh ->query("SELECT HASH,MAX(AUDIT_ID)FROM AUDITLOG GROUP BY AUDIT_ID ORDER BY DATETIME DESC LIMIT 1"); 
	while($row = mysqli_fetch_array($result))
	{	
		$lastHash = $row['HASH']; // Gets the Last hash entered into the database 
	}

	$entry = "$dateTime,$downloads"; // Creates an entry specific to this event
	$entryHash = sha1($lastHash.$entry); // creates the hash off the event 
	
	$result = $dbh->query("SELECT * FROM AUDIT"); 
	$row = $result->fetch_assoc();
	$currentKey = $row['AUDITKEY']; // get the current Audit key out of the database 

	// Note First key should be 5bf1fd927dfb8679496a2e6cf00cbe50c1c87145
	// Creates a hashed log into the log 
	$result = $dbh ->query("INSERT INTO AUDITLOG (DOWNLOADS,USER_ID,BOOK_ID, DATETIME,HASH,
	SIGNATURE) VALUES ('$downloads','$session','$bookid','$dateTime','$entryHash', AES_ENCRYPT('$entryHash','$currentKey'))");
	disconnectFromDatabase($dbh);
}

// Modifiys the Audit Key randomly 
function changeAuditKey()
{		
	if(!isset($_SESSION['userid']))
	{
     		exit('<h2>You cannot access this function directly!</h2>');
	}
	$dbh = connectToDatabase();
	$result = $dbh->query("SELECT * FROM AUDIT"); 
	$row = $result->fetch_assoc();
	$currentKey = $row['AUDITKEY']; // get the current Audit key out of the database 

	//$nextKey = sha1('$currentKey'."Saltinggg"); // Creates the next key using predefined word
	$randomSalt = substr(md5(rand()), 0, 30); // makes a random salt 
	$nextKey = sha1('$currentKey'."$randomSalt"); // creates the next key 
		
	// Changes up the key for the auditkey 
	$result = $dbh ->query("UPDATE AUDIT SET AUDITKEY = '$nextKey' WHERE AUDITKEY = '$currentKey'"); // Replaces the old audit key with new one
	disconnectFromDatabase($dbh);
}

?>