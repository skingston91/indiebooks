<?php
$err = "";
// These are all the functions used for selecting books out of the database
 
//Function for Selecting all books
function selectAllBooks()
{	
	$dbh = connectToDatabase(); 
	$result = $dbh ->query("SELECT * FROM BOOKS");
	return $result;
	disconnectFromDatabase($dbh); // Disconnect the database
}
// Geting the book from an id out of the database
function selectCurrentBook($bookId)
{	
	$dbh = connectToDatabase(); 
	$bookPage= $dbh ->real_escape_string($bookId); // get the id of the book we are editing
	$result = $dbh ->query("SELECT * FROM BOOKS WHERE BOOK_ID = '$bookPage'"); // Get the details of the book
	return $result;
	disconnectFromDatabase($dbh); // Disconnect the database
}

// Function to check if they own the book 
function selectCheckBookOwned($bookPage,$userId)
{	
	$dbh = connectToDatabase(); 
	$bookPage= $dbh ->real_escape_string($bookPage); // get the id of the book 
	$userId = $dbh ->real_escape_string($userId); // get the id of the user
	$result = $dbh ->query("SELECT * FROM BOOKSOWNED WHERE BOOK_ID = '$bookPage' AND USER_ID ='$userId' "); // See if they own the book
	return $result;
	disconnectFromDatabase($dbh); // Disconnect the database
}

//Function to check if the book is confirmed as bought
function selectCheckBookConfirmed($bookId,$userId)
{	
	if(!$_SESSION['userid'] == $userId)
	{
     		exit('<h2>You cannot access this function directly!</h2>');
	}
	$dbh = connectToDatabase(); 
	$bookPage= $dbh ->real_escape_string($bookId); // get the id of the book 
	$userId = $dbh ->real_escape_string($userId); // get the id of the user
	$result = $dbh->query("SELECT * FROM BOOKSOWNED WHERE USER_ID = '$userId' AND BOOK_ID = '$bookId' AND CONFIRMED = '1'");  // Get the details of the book
	return $result;
	disconnectFromDatabase($dbh); // Disconnect the database
}

//Function to check if the book is confirmed as bought
function selectNewBookInsert($bookId,$userId)
{	
	if(!$_SESSION['userid'] == $userId)
	{
     		exit('<h2>You cannot access this function directly!</h2>');
	}
	$dbh = connectToDatabase(); 
	$bookPage= $dbh ->real_escape_string($bookId); // get the id of the book 
	$userId = $dbh ->real_escape_string($userId); // get the id of the user
	$result = $dbh->query("SELECT * FROM BOOKSOWNED WHERE USER_ID = '$userId' AND BOOK_ID = '$bookId'");  // Get the details of the book
	return $result;
	disconnectFromDatabase($dbh); // Disconnect the database
}
// Function for checking if the key given matches the one stored 
function selectCheckKeyMatch($bookId,$userId,$link)
{	
	if(!$_SESSION['userid'] == $userId)
	{
     		exit('<h2>You cannot access this function directly!</h2>');
	}
	$dbh = connectToDatabase(); 
	$bookId= $dbh ->real_escape_string($bookId); // get the id of the book we are editing
	$userId = $dbh ->real_escape_string($userId); // make sure the id is escaped
	$link = $dbh ->real_escape_string($link); // make sure the link is escaped
	$result = $dbh->query("SELECT * FROM BOOKSOWNED WHERE USER_ID = '$userId' AND BOOK_ID = '$bookId' AND LINK ='$link'"); 
	return $result;
	disconnectFromDatabase($dbh); // Disconnect the database
}

// For selecting the newest bought book 
function selectBoughtBook($userId,$link)
{
	if(!$_SESSION['userid'] == $userId)
	{
     		exit('<h2>You cannot access this function directly!</h2>');
	}
	$dbh = connectToDatabase(); 
	$userId= $dbh ->real_escape_string($userId); // get the id of the user
	$link = $dbh ->real_escape_string($link); // make sure the link is escaped
	$result = $dbh ->query("SELECT bo.BOOK_ID,bo.LINK,b.TITLE,b.AUTHOR,b.YEAR FROM BOOKSOWNED bo
	INNER JOIN USERS u
	ON bo.USER_ID = u.USER_ID
	INNER JOIN BOOKS b
	ON b.BOOK_ID = bo.BOOK_ID
	WHERE u.USER_ID ='$userId'
	AND bo.LINK ='$link'
	AND bo.CONFIRMED='1'");
	return $result;
	disconnectFromDatabase($dbh); // Disconnect the database
}

// For selecting all the users books  
function getAllBoughtBooks($session)
{
	if(!$_SESSION['userid'] == $session)
	{
     		exit('<h2>You cannot access this function directly!</h2>');
	}
	$dbh = connectToDatabase(); 
	$session= $dbh ->real_escape_string($session); // get the id of the user
	$result = $dbh ->query("SELECT bo.BOOK_ID,bo.LINK,b.TITLE,b.AUTHOR,b.YEAR FROM BOOKSOWNED bo
	INNER JOIN USERS u
	ON bo.USER_ID = u.USER_ID
	INNER JOIN BOOKS b
	ON b.BOOK_ID = bo.BOOK_ID
	WHERE u.USER_ID ='$session'
	AND bo.CONFIRMED = '1'");
	return $result;
	disconnectFromDatabase($dbh); // Disconnect the database
}

// For selecting all unverfied users books  
function getAllUnverfiedBooks($session)
{
	if(!$_SESSION['userid'] == $session)
	{
     		exit('<h2>You cannot access this function directly!</h2>');
	}
	$dbh = connectToDatabase(); 
	$session= $dbh ->real_escape_string($session); // get the id of the user
	$result = $dbh ->query("SELECT bo.BOOK_ID,bo.LINK,b.TITLE,b.AUTHOR,b.YEAR FROM BOOKSOWNED bo
	INNER JOIN USERS u
	ON bo.USER_ID = u.USER_ID
	INNER JOIN BOOKS b
	ON b.BOOK_ID = bo.BOOK_ID
	WHERE u.USER_ID ='$session'
	AND bo.CONFIRMED = '0'");
	return $result;
	disconnectFromDatabase($dbh); // Disconnect the database
}

// For selecting all users books  
function getUserBooks($session)
{
	if(!$_SESSION['userid'] == $session)
	{
     		exit('<h2>You cannot access this function directly!</h2>');
	}
	$dbh = connectToDatabase(); 
	$session= $dbh ->real_escape_string($session); // get the id of the user
	$result = $dbh ->query("SELECT bo.BOOK_ID,bo.LINK,b.TITLE,b.AUTHOR,b.YEAR FROM BOOKSOWNED bo
	INNER JOIN USERS u
	ON bo.USER_ID = u.USER_ID
	INNER JOIN BOOKS b
	ON b.BOOK_ID = bo.BOOK_ID
	WHERE u.USER_ID ='$session'");
	return $result;
	disconnectFromDatabase($dbh); // Disconnect the database
}
?>