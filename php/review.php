<?php
// Function for adding reviews to database 
session_start();
require '../config.php';

	if(!$_SESSION['userid'])
	{
     		exit('<h2>You cannot access this function directly!</h2>');
	}
		
	$dbh = connectToDatabase();
	$_POST['comments'] = $dbh ->real_escape_string($_POST['comments']);
	$userid =$dbh ->real_escape_string($_SESSION['userid']);
	$bookPage =$dbh ->real_escape_string($_GET["id"]);
	
	$result = $dbh ->query("INSERT INTO REVIEWS(CONTENT,RATING,USER_ID,BOOK_ID) VALUES('{$_POST['comments']}','{$_POST['rating']}','{$userid}','{$bookPage}')");

	disconnectFromDatabase($dbh);	
	header("Location: ../books.php?id=$bookPage");
?>