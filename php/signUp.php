<?php
// Code for the sign up screen and hashing passwords
session_start();
require '../config.php';
CRYPT_BLOWFISH or die ('No Blowfish found.');

	// Checking whether the Login form has been fully filled in
	if(!$_POST['username']|| !$_POST['firstname']|| !$_POST['lastname'] || !$_POST['password']|| !$_POST['email'])
	{
		$err = 'All the fields must be filled in!';
		$_SESSION['error'] = $err;
		header("Location: ../signUp.php");
	}	
	else
	{
		$dbh = connectToDatabase(); // connect to database
		//This string tells crypt to use blowfish for 5 rounds.
		$blowfishPre = '$2a$05$';
		$blowfishEnd = '$';
		
		// Blowfish uses these characters for salts.
		$allowedChars ='ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789./';
		$charsLength = 63; 
		
		// Length of the salt
		$saltLength = 21;
		$salt = "";
		
		for($i=0; $i<$saltLength; $i++)
		{
			$salt .= $allowedChars[mt_rand(0,$charsLength)];
		}
		$bcrypt_salt = $blowfishPre . $salt . $blowfishEnd; // creates the salt 
		
		// Escaping all input data
		$_POST['username'] = $dbh ->real_escape_string($_POST['username']);
		$_POST['firstname'] = $dbh ->real_escape_string($_POST['firstname']);
		$_POST['lastname'] = $dbh ->real_escape_string($_POST['lastname']);
		$password = $dbh ->real_escape_string($_POST['password']);
		$_POST['email'] = $dbh ->real_escape_string($_POST['email']);
		
		$hashedPass = crypt($password, $bcrypt_salt); // encypts with the salt and password
		
		$result = $dbh ->query("INSERT INTO USERS(USERNAME,FIRSTNAME,LASTNAME,PASSWORD,EMAIL,ADMIN,SALT)
						VALUES(
							'{$_POST['username']}',
							'{$_POST['firstname']}',
							'{$_POST['lastname']}',
							'$hashedPass',
							'{$_POST['email']}',
							'0',
							'$salt'
				)");	
				
		$err = 'Sign Up Success Please Login';
		$_SESSION['error'] = $err;
		disconnectFromDatabase($dbh);
		header("Location: ../login.php");

	}


?>	