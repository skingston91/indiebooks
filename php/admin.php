<?php 
// These are all the admin functions used when adding a new book or updating a new book or deleting a book 

session_start();
require '../config.php';
$err = "";
if($_GET['Action']== 'add'&& $_SESSION['admin'] == 1) // Function to add a new book to the database
	{
		//Code for adding a new book txt file
		if($_FILES['file']['type'] == 'text/plain')// check if correct format
		{
			if ($_FILES["file"]["error"] > 0) // check for errors
			{
				$err="Error: " . $_FILES["file"]["error"] . "<br>";
			}
			else
			{
				$dbh = connectToDatabase(); // Code for adding a book to database
				$_POST['title'] = $dbh ->real_escape_string($_POST['title']);
				$_POST['author'] = $dbh ->real_escape_string($_POST['author']);
				$_POST['year'] = $dbh ->real_escape_string($_POST['year']);
				$_POST['description'] = $dbh ->real_escape_string($_POST['description']);
				$_POST['price'] = $dbh ->real_escape_string($_POST['price']);
				$filename = $_POST['title'].".".$_POST['year'];
		
				$result = $dbh ->query("INSERT INTO BOOKS(TITLE,AUTHOR,YEAR,DESCRIPTION,PRICE,FILENAME) VALUES('{$_POST['title']}','{$_POST['author']}','{$_POST['year']}','{$_POST['description']}','{$_POST['price']}','$filename')");
						
				$result1 = $dbh ->query("SELECT MAX(BOOK_ID) AS BOOKID from BOOKS"); // get the latest id so the book we just added
				$row = $result1->fetch_assoc(); // get row out of the database
				$bookId = $row['BOOKID']; // get the id of the book for naming
				$bookId = $bookId.".txt"; // only text files allowed so add the prefix
				disconnectFromDatabase($dbh); // get of the database
				
				move_uploaded_file($_FILES["file"]["tmp_name"],"../books/" . $bookId); // move the temp file to the books folder under its new name 
				$err= " Book Added and Stored in: " . "" . $_FILES["file"]["name"]; // State the book has been added and the id
			}
		}
		else
		{
			$err="Not a text file";

		}
		header("Location: ../admin.php");
		$_SESSION['error'] = $err;	 
	}
	
if($_GET['Action']== 'update'&& $_SESSION['admin'] == 1) // Function to edit a book and database in the database
	{	
		// This section changes the database
		$dbh = connectToDatabase(); // connects to the database
		$id= $dbh ->real_escape_string($_POST['bookid']);
		$title = $dbh ->real_escape_string($_POST['title']);
		$author = $dbh ->real_escape_string($_POST['author']);
		$year = $dbh ->real_escape_string($_POST['year']);
		$description = $dbh ->real_escape_string($_POST['description']);
		$price= $dbh ->real_escape_string($_POST['price']);
		$filename = $_POST['title'].".".$_POST['year'];
		
		// Edit the table 
		$result = $dbh ->query("UPDATE BOOKS SET TITLE = '$title',AUTHOR = '$author',YEAR = '$year',DESCRIPTION = '$description',PRICE = '$price',FILENAME = '$filename' WHERE BOOK_ID ='$id'");
		disconnectFromDatabase($dbh);
				
		$err="Book Settings Updated";
	
		if($_FILES['file']['size'] > 0) // there is a file it should replace the file
		{
			if($_FILES['file']['type'] == 'text/plain')// check if correct format
			{
				if ($_FILES["file"]["error"] > 0) // check for errors
				{
					$err="Error: " . $_FILES["file"]["error"] . "<br>";
				}
				else // will always end up inserting the book into the database
				{
					$bookId = $id;
					$bookId = $bookId.".txt"; // set the name with prefix file
					
					if (file_exists("../books/" . $bookId))// if the file already exits which it should always be true but just in case
					{
						unlink("../books/" . $bookId); // delete the old book text file
						move_uploaded_file($_FILES["file"]["tmp_name"],"../books/" . $bookId); // move the temp file to the books folder under its new name 
						$err= " Book Settings and Book text replaced ";
					}
					else // insert the book because nothing was there before 
					{
						move_uploaded_file($_FILES["file"]["tmp_name"],"../books/" . $bookId); // move the temp file to the books folder under its new name 
						$err= "Book Settings and Book text Added"; 
					}
				}
			}	
			else	
			{
				$err="Not a text file"; // Not a text file so it can't be added 
			}		
		}
		$_SESSION['error'] = $err;
		header("Location: ../admin.php");
	}
	
if($_GET['Action']== 'delete'&& $_SESSION['admin'] == 1) // Function to add a new book to the database
	{
		$dbh = connectToDatabase();
		$id= $dbh ->real_escape_string($_GET['id']);
		
		$result = $dbh ->query("DELETE FROM BOOKS WHERE BOOK_ID ='$id'");
		$err= "Book Deleted";
		
		$bookId = $id;
		$bookId = $bookId.".txt"; // set the name with prefix file
		if (file_exists("../books/" . $bookId))// if the file already exits which it should always be true but just in case
		{
			unlink("../books/" . $bookId); // delete the old book text file
			$err= "Book Setings and Book Text Deleted";
		}
		
		$_SESSION['error'] = $err;
		disconnectFromDatabase($dbh);
		header("Location: ../admin.php");
	}
elseif(!$_SESSION['admin'] == 1)// if not an admin send them back to the main page
	{
		header("Location: ../index.php");
		$err="You are not an ADMIN!";
		$_SESSION['error'] = $err;
	}		
?>




