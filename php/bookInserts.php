<?php
$err = "";
// Function for dealing with bookowned

//Function for inserting an unconfirmed book
function insertUnConfirmedBook($userId,$bookId,$link)
{	
	if(!$_SESSION['userid'] == $userId)
	{
     		exit('<h2>You cannot access this function directly!</h2>');
	}
	$dbh = connectToDatabase(); 
	$userId= $dbh ->real_escape_string($userId); // get the user id of the person buying
	$bookId= $dbh ->real_escape_string($bookId); // get the id of the book we are inserting
	$link = $dbh ->real_escape_string($link); // get the link
	$result = $dbh ->query("INSERT INTO BOOKSOWNED(USER_ID,BOOK_ID,LINK,CONFIRMED) VALUES('$userId','$bookId','$link','0')");
	disconnectFromDatabase($dbh); // Disconnect the database
}

//Function for inserting that someone now owns a book into bookowned 
function updateToConfirmedBook($userId,$bookId,$link)
{	
	if(!$_SESSION['userid'] == $userId)
	{
     		exit('<h2>You cannot access this function directly!</h2>');
	}
	$dbh = connectToDatabase(); 
	$userId= $dbh ->real_escape_string($userId); // get the user id of the person buying
	$bookId= $dbh ->real_escape_string($bookId); // get the id of the book we are inserting
	$link = $dbh ->real_escape_string($link); // get the link
	$result = $dbh ->query("UPDATE BOOKSOWNED SET CONFIRMED = '1' WHERE USER_ID = '$userId' AND BOOK_ID = '$bookId' AND LINK = '$link'");
	disconnectFromDatabase($dbh); // Disconnect the database
}

?>