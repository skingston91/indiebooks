<?php
// Code for logging onto the system 

// Start the session
session_start();
require '../config.php';
$err = "";
CRYPT_BLOWFISH or die ('No Blowfish found.'); // if the blowfish isn't found in the php then do nothing

	if(!$_POST['username'] || !$_POST['password']) // if the form is not filled in 
	{
		$err = 'All the fields must be filled in!'; // add the error
		header("Location: ../login.php"); // send them back to the log in page
		$_SESSION['error'] = $err; // set the error as the session
	}	
	else
	{
	
		$dbh = connectToDatabase(); // start the database connection
		// Escaping all input data
		$_POST['username'] = $dbh ->real_escape_string($_POST['username']); 
		$password = $dbh ->real_escape_string($_POST['password']);
		
		//This string tells crypt to use blowfish for 5 rounds.
		$Blowfish_Pre = '$2a$05$';
		$Blowfish_End = '$';
		
		//gets the current salt
		$userResult = $dbh->query("SELECT SALT FROM USERS WHERE USERNAME='{$_POST['username']}'");
		$row_cnt = mysqli_num_rows($userResult); // count the amount of rows
		
		if($row_cnt == 1) // if its one then set the variables because matching user
		{
			$row = $userResult->fetch_assoc();			
			$hashedPass = crypt($password, $Blowfish_Pre . $row['SALT'] . $Blowfish_End); // create our hashed pass using the users salt taken from the database
		}
		else // Fails and then sends them the error
		{
			disconnectFromDatabase($dbh);
			header("Location: ../login.php");
			$err= "No Login found for that name";
			$_SESSION['error'] = $err;
		}
		
		// Code to see if the password and username match up 
		$result = $dbh->query("SELECT USER_ID,USERNAME,ADMIN FROM USERS WHERE USERNAME='{$_POST['username']}' AND PASSWORD='$hashedPass'");
		$row_cnt = mysqli_num_rows($result); // count the amount of rows
		
		if($row_cnt == 1) // if its one then set the variables as matching pass and user
		{
			// If everything is OK login
			$row = $result->fetch_assoc();			
			$_SESSION['userid']= $row['USER_ID'];	// set the session from the database
			$_SESSION['username']=$row['USERNAME'];	
			$_SESSION['admin']= $row['ADMIN'];
			$_SESSION['loggedIn'] = true; // set a logged in session value
			disconnectFromDatabase($dbh);	
			header("Location: ../index.php"); // redirect the user
			$err="Login Success";
			$_SESSION['error'] = $err;
		}
		else // Fails and then sends them the error
		{
			disconnectFromDatabase($dbh);
			header("Location: ../login.php");
			$err="Password is incorrect";
			$_SESSION['error'] = $err;
		}
	disconnectFromDatabase($dbh);	
	}
?>	