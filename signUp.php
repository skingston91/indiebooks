<?php
// The page for signing up users

// Start the Session
session_start();
require 'config.php';

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
  <head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    
	<title>In-di-eBooks</title>
	<!-- <link rel="stylesheet" type="text/css" href="css/main.css" /> -->
    <link href="css/jumbotron-narrow.css" rel="stylesheet">
	
   <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet"/>
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
	
</head>
<body>
	<div class="container">
      <div class="header clearfix">
        <nav>
          <ul class="nav nav-pills pull-right">
			<li role="presentation" ><a href="index.php">Home</a></li>
			<?php	// If the user is logged in then don't output the login and sign in links	
				if(!isset($_SESSION['loggedIn'])) { ?>
				<li role="presentation"><a href="login.php">Login </a></li>
				<li role="presentation" class = "active"><a href="signUp.php"> Sign Up </a></li>
			<?php }
			else //if they logged in show them the logOut link
			{	?>
				<li role="presentation" ><a href="users.php"> My Profile </a></li>
				<li role="presentation"><a href="logout.php"> Logout </a></li>
			<?php } ?>
        </nav>
        <h3 class="text-muted">In-di-eBooks</h3>
	</div>
	  
<?php
	// Output the Error if there is one
	if(isset($_SESSION['error']))
	{
		echo '<div class="err">'.$_SESSION['error'].'</div>';
		unset($_SESSION['error']);
	}
?>
<body>

	<form action="php/signUp.php" method="post">
		<label>Username:</label>
		<input type="text" name="username" id="username"/>
		<label>First Name:</label>
		<input type="text" name="firstname" id="firstname"/>
		<label>Last Name:</label>
		<input type="text" name="lastname" id="lastname"/>
		<label>Password:</label>
		<input type="password" name="password" id="password" />
		<label>Email:</label>
		<input type="text" name="email" id="email"/>
		<input type="submit" name="submit" value="Signup" />
	</form>
	
	<script src="js/jquery-1.8.3.min.js"></script>
</body>       
</html>