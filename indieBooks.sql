/*
SQLyog Community v11.01 (32 bit)
MySQL - 5.0.45-log : Database - indiebooks_1
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

/* I've added these in manually  */
CREATE DATABASE /*!32312 IF NOT EXISTS*/`indiebooks_1` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `indiebooks_1`;
/*Table structure for table `AUDIT` */

DROP TABLE IF EXISTS `AUDIT`;

CREATE TABLE `AUDIT` (
  `AUDITKEY` varchar(255) default NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

/*Data for the table `AUDIT` */

insert  into `AUDIT`(`AUDITKEY`) values ('cc3fdaea63d906ec71aec9c296dc957b12597e06');

/*Table structure for table `AUDITLOG` */

DROP TABLE IF EXISTS `AUDITLOG`;

CREATE TABLE `AUDITLOG` (
  `AUDIT_ID` int(11) NOT NULL auto_increment,
  `DOWNLOADS` int(11) NOT NULL,
  `USER_ID` int(11) NOT NULL,
  `BOOK_ID` int(11) NOT NULL,
  `DATETIME` datetime NOT NULL,
  `HASH` varchar(255) NOT NULL,
  `SIGNATURE` varchar(255) NOT NULL,
  PRIMARY KEY  (`AUDIT_ID`),
  KEY `USER_ID` (`USER_ID`),
  KEY `BOOK_ID` (`BOOK_ID`),
  CONSTRAINT `AUDITLOG_ibfk_1` FOREIGN KEY (`USER_ID`) REFERENCES `USERS` (`USER_ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `AUDITLOG_ibfk_2` FOREIGN KEY (`BOOK_ID`) REFERENCES `BOOKS` (`BOOK_ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ;

/*Data for the table `AUDITLOG` */

insert  into `AUDITLOG`(`AUDIT_ID`,`DOWNLOADS`,`USER_ID`,`BOOK_ID`,`DATETIME`,`HASH`,`SIGNATURE`) values (1,0,4,1,'2013-04-17 22:18:35','c1d7dfe03863f750ce53e0e51cfbf60c84337f4d','');

/*Table structure for table `BOOKS` */

DROP TABLE IF EXISTS `BOOKS`;

CREATE TABLE `BOOKS` (
  `BOOK_ID` int(11) NOT NULL auto_increment,
  `TITLE` varchar(100) NOT NULL,
  `AUTHOR` varchar(50) NOT NULL,
  `YEAR` int(4) default NULL,
  `DESCRIPTION` varchar(1000) default NULL,
  `PRICE` decimal(10,2) NOT NULL default '0.00',
  `FILENAME` varchar(500) NOT NULL default '',
  PRIMARY KEY  (`BOOK_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

/*Data for the table `BOOKS` */

insert  into `BOOKS`(`BOOK_ID`,`TITLE`,`AUTHOR`,`YEAR`,`DESCRIPTION`,`PRICE`,`FILENAME`) values (1,'HeadHunters','Jo Nesbo',2012,'Roger Brown has it all: clever and wealthy, he\'s at the very top of his game. And if his job as a headhunter ever gets dull, he has his sideline as an art thief to keep him busy.  At a gallery opening, his wife introduces him to Clas Greve. Not only is Greve the perfect candidate for a position that Brown is recruiting for; he is also in possession of one of the most sought-after paintings in mode',3.86,'HeadHunters.2012'),(2,'The Hobbit','J.R.R Tolkien',1937,'Bilbo Baggins is a hobbit who enjoys a comfortable, unambitious life, rarely travelling further than the pantry of his hobbit-hole in Bag End.  But his contentment is disturbed when the wizard, Gandalf, and a company of thirteen dwarves arrive on his doorstep one day to whisk him away on an unexpected journey ‘there and back again’. They have a plot to raid the treasure hoard of Smaug the Magnific',3.84,'The Hobbit.1937'),(6,'Game of Thrones','George R.R Martin',2013,'A STORM OF SWORDS: STEEL AND SNOW is the FIRST part of the third volume in the series.  Winter approaches Westeros like an angry beast.  The Seven Kingdoms are divided by revolt and blood feud. In the northern wastes, a horde of hungry, savage people steeped in the dark magic of the wilderness is poised to invade the Kingdom of the North where Robb Stark wears his new-forged crown. And Robb’s defences are ranged against the South, the land of the cunning and cruel Lannisters, who have his younger sisters in their power.  Throughout Westeros, the war for the Iron Throne rages more fiercely than ever, but if the Wall is breached, no king will live to claim it.',3.85,'Game of Thrones.2013'),(7,'Phantom: A Harry Hole thriller','Nesbo Jo',2013,'After the horrors of a case that nearly cost him his life, Harry Hole left Oslo and the police force far behind him. Now he\'s back, but the case he\'s come to investigate is already closed, and the suspect already behind bars.  THE POLICE DON\'T WANT HIM BACK...  Denied permission to reopen the investigation, Harry strikes out on his own, quickly discovering a trail of violence and mysterious disappearances apparently unnoticed by the police. At every turn, Harry is faced with a wall of silence. ',3.84,'Phantom: A Harry Hole thriller.2013');

/*Table structure for table `BOOKSOWNED` */

DROP TABLE IF EXISTS `BOOKSOWNED`;

CREATE TABLE `BOOKSOWNED` (
  `BOOKOWNED_ID` int(11) NOT NULL auto_increment,
  `USER_ID` int(11) default NULL,
  `BOOK_ID` int(11) default NULL,
  `LINK` varchar(500) NOT NULL,
  `CONFIRMED` tinyint(1) NOT NULL default '0',
  PRIMARY KEY  (`BOOKOWNED_ID`),
  KEY `USER_ID` (`USER_ID`),
  KEY `BOOK_ID` (`BOOK_ID`),
  CONSTRAINT `BOOKSOWNED_ibfk_1` FOREIGN KEY (`USER_ID`) REFERENCES `USERS` (`USER_ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `BOOKSOWNED_ibfk_2` FOREIGN KEY (`BOOK_ID`) REFERENCES `BOOKS` (`BOOK_ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

/*Data for the table `BOOKSOWNED` */

insert  into `BOOKSOWNED`(`BOOKOWNED_ID`,`USER_ID`,`BOOK_ID`,`LINK`,`CONFIRMED`) values (1,4,1,'de8af113744ca70c9222c5840de91b',1),(2,4,2,'8a0156e4388ca3d53b2945cd1ddaa2',1),(3,2,1,'efa02d010b41945ff39e5e1d9c2c95',1),(4,1,1,'248b47b70eaba48769846125257b3b',1),(5,1,2,'1de681ba1c4e99f0faa889265150fc',1),(6,1,6,'5382bbb3f053af2dbe215293d769c7',1),(7,1,7,'834313218170e1df96a08b94e96f1b',1),(8,2,2,'c4edd9b1e806a6484f6d7bcbd9048a',1);

/*Table structure for table `REVIEWS` */

DROP TABLE IF EXISTS `REVIEWS`;

CREATE TABLE `REVIEWS` (
  `REVIEW_ID` int(11) NOT NULL auto_increment,
  `CONTENT` varchar(500) NOT NULL,
  `RATING` int(1) default NULL,
  `USER_ID` int(11) default NULL,
  `BOOK_ID` int(11) default NULL,
  PRIMARY KEY  (`REVIEW_ID`),
  KEY `USER_ID` (`USER_ID`),
  KEY `BOOK_ID` (`BOOK_ID`),
  CONSTRAINT `REVIEWS_ibfk_1` FOREIGN KEY (`USER_ID`) REFERENCES `USERS` (`USER_ID`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `REVIEWS_ibfk_2` FOREIGN KEY (`BOOK_ID`) REFERENCES `BOOKS` (`BOOK_ID`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

/*Data for the table `REVIEWS` */

insert  into `REVIEWS`(`REVIEW_ID`,`CONTENT`,`RATING`,`USER_ID`,`BOOK_ID`) values (1,' This book is awesome but abit short',4,1,1),(2,' This book is stranggeeeee',4,2,1),(3,' I was not a Fan of this book ',1,1,1),(4,' index.php?name=guest<script>alert(\'attacked\')</script>',0,1,1),(5,'I need to read this book!!',5,1,7),(6,' Mannn I havent read this since I was like 5',5,1,2);

/*Table structure for table `USERS` */

DROP TABLE IF EXISTS `USERS`;

CREATE TABLE `USERS` (
  `USER_ID` int(11) NOT NULL auto_increment,
  `USERNAME` varchar(40) NOT NULL default '',
  `FIRSTNAME` varchar(40) NOT NULL default '',
  `LASTNAME` varchar(40) NOT NULL default '',
  `PASSWORD` char(60) NOT NULL default '',
  `EMAIL` varchar(80) NOT NULL default '',
  `ADMIN` tinyint(1) NOT NULL default '0',
  `SALT` char(21) NOT NULL,
  PRIMARY KEY  (`USER_ID`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

/*Data for the table `USERS` */

insert  into `USERS`(`USER_ID`,`USERNAME`,`FIRSTNAME`,`LASTNAME`,`PASSWORD`,`EMAIL`,`ADMIN`,`SALT`) values (1,'Admin','Admin','strator','$2a$05$rvLzj5vwoXiCuQvll6MjE.tvsGE3vUi84ciYEVzphT1ca2xyfDtCK','asdasd',1,'rvLzj5vwoXiCuQvll6MjE'),(2,'Test','Test','ing','$2a$05$7N.PSdpglkIvVsvMc/2Af.IivxPrq07mG1VSgGLgzZ8wTTrYGJNwm','test',0,'7N.PSdpglkIvVsvMc/2Af'),(4,'user ','us','er','$2a$05$3cy.e8gR9nYJwXy7kG0Kb.PVilc/4ID/zo9DK3p8rSBwg0bmlndcu','asdasd',0,'3cy.e8gR9nYJwXy7kG0Kb'),(10,'FinalTest ','Final','Test','$2a$05$75ti2L29l1B2Izv03W32u.cR2cP4iFJ6nz7a5CRwUYAoih2g1oMU6','test',0,'75ti2L29l1B2Izv03W32u');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
