<?php
//This Page is for showing is allowing for people to buy a book and showing reviews
 
// Starting the session
session_start();
require 'config.php';
include 'php/bookSelects.php';
include 'php/reviewSelect.php';
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
  <head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    
	<title>In-di-eBooks</title>
	<!-- <link rel="stylesheet" type="text/css" href="css/main.css" /> -->
    <link href="css/jumbotron-narrow.css" rel="stylesheet">
	
   <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet"/>
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
	
</head>

<body>
<div class="container">
      <div class="header clearfix">
        <nav>
          <ul class="nav nav-pills pull-right">
			<li role="presentation" ><a href="index.php">Home</a></li>
			<?php	// If the User is logged in as an admin and they have a session for admin value then show them the admin link 
				if(isset($_SESSION['admin']) && $_SESSION['admin'] == 1) { ?>
				<li role="presentation"><a href="admin.php"> Admin </a></li>
				<li role="presentation"><a href= "auditLog.php"> Audit Log </a></li>
			<?php } ?>
			<?php	// If the user is logged in then don't output the login and sign in links	
				if(!isset($_SESSION['loggedIn'])) { ?>
				<li role="presentation"><a href="login.php">Login </a></li>
				<li role="presentation"><a href="signUp.php"> Sign Up </a></li>
			<?php }
			else //if they logged in show them the logOut link
			{	?>
				<li role="presentation" ><a href="users.php"> My Profile </a></li>
				<li role="presentation"><a href="logout.php"> Logout </a></li>
			<?php } ?>
          </ul>
        </nav>
        <h3 class="text-muted">In-di-eBooks</h3>
      </div>

<?php
	
	
	echo "<h3>Welcome to the page of....</h3>";

	if(isset($_SESSION['userid']))// if the user is logged in and has an id 
	{
		$userId = $_SESSION['userid']; 
	}
	elseif(!isset($_SESSION['userid'])) // if it hasn't been set just make the user 0 which doesn't exist 
	{
		$userId = 0;
	}
	$bookPage = $_GET['id']; // set the id of the book 
	$result = selectCurrentBook($bookPage); // Get the current book details
	while($row = mysqli_fetch_array($result))
	{	
		$title = htmlspecialchars($row['TITLE']);
		$author = htmlspecialchars($row['AUTHOR']);
		$price = htmlspecialchars($row['PRICE']);
		
		echo  htmlspecialchars($row['TITLE']) . " " .  htmlspecialchars($row['AUTHOR']). " " .  htmlspecialchars($row['YEAR']). " �" . htmlspecialchars($row['PRICE']);
		echo "<br />";
		echo  $row['DESCRIPTION'];
	}

	
	$result = selectCheckBookOwned($bookPage,$userId);// check if the book is already owned by the user
	while($row = mysqli_fetch_array($result))
	{	
		$confirmed = $row['CONFIRMED'];
	}	

	if(isset($_SESSION['loggedIn']) && (!isset($confirmed))) // Can only buy if logged in and don't already own it
	{		
		echo "<form action='checkout.php' method='POST'  enctype='multipart/form-data' >
			<input type='hidden' name='bookid' id='bookid' value= '$bookPage' />
			<input type='hidden' name='title' id='title' value= '$title' />
			<input type='hidden' name='author' id='author' value= '$author' />
			<input type='hidden' name='price' id='price' value= '$price' />
			<input type='hidden' name='currency' id='currency' value='GBP'/>
			<input type='hidden' name='quantity' id='quantity' value='1'/>
			<input type='image' name='Google Checkout' alt='Fast checkout through Google'
			src='http://upload.wikimedia.org/wikipedia/en/e/e3/Google_Checkout.png' height='46' width='200'>
			</form>";  
	}
	echo "</br>";

	if(isset($_SESSION['loggedIn']) && $_SESSION['admin'] == 1) // If they are an admin user then let them see the edit and delete buttons
	{
		echo ' <a href=adminUpdate.php?id=' . $bookPage . '>Update Book </a>'; // The Edit Page for the book
		echo ' <a href=php/admin.php?id='. $bookPage.'&Action=delete>Delete Book </a>'; // The delete book link
	}
	else // do nothing 
	{
		
	}
		
	if(!isset($_SESSION['loggedIn'])) // If they are not logged in tell them they can't comment and don't let them comment
	{
		echo '<h4> Please Log in To Comment And Buy books</h4>'; 
 	}
	else // let them comment
	{
		// Set the values of the comment and review score
		echo '<form action="php/review.php?id=' . $bookPage . '" method="POST">';
		echo '<div class="comment">'; 
		echo '<h4 id="commentHeading">Comments:</h4>';
		echo '<p>Rating</p>';
		echo '<select name = "rating">
			<option value="0">0</option>
			<option value="1">1</option>
			<option value="2">2</option>
			<option value="3">3</option>
			<option value="4">4</option>
			<option value="5">5</option>
			</select>';
		echo '<textarea name="comments" id= "commentBox" > </textarea>';
		echo "<br />";
		echo '<button action="commentButton" id= "commentSubmit">Add Comment</button>';
		echo '</div> </form>' ;
	}	

	echo "<h4> Reviews  </h4>";
	

	// Get all the users and their names and the comments for this article 	
	$result = getReviews($bookPage);	
	// Output the results of the query
	while($row = mysqli_fetch_array($result))
	{	
		echo htmlspecialchars($row['FIRSTNAME']). " " . htmlspecialchars($row['LASTNAME']);	
		echo "<br />";
		echo htmlspecialchars($row['CONTENT']) ;
		echo "<br />";
		echo "Star Rating of: " . htmlspecialchars($row['RATING']) ;
		echo "<br />";
		echo "<br />";
	}	
?>
</body>       
</html>