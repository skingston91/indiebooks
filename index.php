<?php
// Starting the session
session_start();
require 'config.php';
include 'php/bookSelects.php';
/*
	index.php - This is the main page that gets opened when a user opens the site
	- This redirects users to log on pages and to books

*/
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
  <head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    
	<title>In-di-eBooks</title>
	<!-- <link rel="stylesheet" type="text/css" href="css/main.css" /> -->
    <link href="css/jumbotron-narrow.css" rel="stylesheet">
	
   <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet"/>
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
	
</head>
<body>
	<div class="container">
      <div class="header clearfix">
        <nav>
          <ul class="nav nav-pills pull-right">
			<li role="presentation" class="active"><a href="index.php">Home</a></li>
			<?php	// If the User is logged in as an admin and they have a session for admin value then show them the admin link 
				if(isset($_SESSION['admin']) && $_SESSION['admin'] == 1) { ?>
				<li role="presentation"><a href="admin.php"> Admin </a></li>
				<li role="presentation"><a href= "auditLog.php"> Audit Log </a></li>
			<?php } ?>
			<?php	// If the user is logged in then don't output the login and sign in links	
				if(!isset($_SESSION['loggedIn'])) { ?>
				<li role="presentation"><a href="login.php">Login </a></li>
				<li role="presentation"><a href="signUp.php"> Sign Up </a></li>
			<?php }
			else //if they logged in show them the logOut link
			{	?>
				<li role="presentation"><a href="users.php"> My Profile </a></li>
				<li role="presentation"><a href="logout.php"> Logout </a></li>
			<?php } ?>
          </ul>
        </nav>
        <h3 class="text-muted">In-di-eBooks</h3>
      </div>
	  
	<div class="jumbotron">
        <h1>In-di-eBooks</h1>
        <p class="lead">Welcome to the In-di-eBooks Store</p>
        <p class="lead">This is a place for people to view books and add reviews </p>
		<p>Admin Login: Admin/Password</p>
		<p>Standard Login: Sign up and in!</p>
    </div>
	

<?php
// Code for outputting messages to the user when they get passed around the site
	if(isset($_SESSION['error']))
	{
		echo '<div class="err">'.$_SESSION['error'].'</div>';
		unset($_SESSION['error']);
	}
?>

<?php	

	// Output all the books in the database 
	$result = selectAllBooks();
	while($row = mysqli_fetch_array($result))
	{	
		echo  htmlspecialchars($row['TITLE']) . " " .  htmlspecialchars($row['AUTHOR']). " " .  htmlspecialchars($row['YEAR']). " �" . htmlspecialchars($row['PRICE']);
		echo ' <a href=books.php?id=' .  htmlspecialchars($row['BOOK_ID']) . '>Buy </a>'; // The link for the books page
		echo "<br />";
		echo  $row['DESCRIPTION'];
		echo "<br />";
		echo "<br />";
	}
		
?>
	<footer>
		 <nav class="navbar navbar-fixed-bottom">
			<div class="container-fluid">
			<p>&copy; 2015 SKDev. All rights reserved.</p>
			</div><!-- /.container-fluid -->
		 </nav> 
		
		<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
		
		<!-- Include all compiled plugins (below), or include individual files as needed -->
		<script src="js/bootstrap.min.js"></script>
		
	
	</footer>
</body>       
</html>